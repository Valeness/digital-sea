<?php
require __DIR__ . '/vendor/autoload.php';

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

session_start();

class Request {

    protected $headers = [];
    protected $ch;

    public function __construct($url) {
        $this->setup($url);
    }

    public function setHeader($name, $value) {
        $this->headers[$name] = $value;
    }

    public function setup($url) {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    }

    public function call() {
        $formatted_headers = [];

        foreach($this->headers as $name => $header) {
            $formatted_headers[] = $name . ': ' . $header;
        }

        if(!empty($formatted_headers)) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $formatted_headers);
        }

        $out = curl_exec($this->ch);

        if(empty($out)) {

        }

        return json_decode($out, true);
    }

    public function get() {
        return $this->call();
    }

    public function post($data) {
        curl_setopt($this->ch,CURLOPT_POST, 1);
        curl_setopt($this->ch,CURLOPT_POSTFIELDS, http_build_query($data));
        return $this->call();
    }
}

class Driplet {

    public $mem;
    public $swap;
    public $cpu;
    public $disk;

    public function __construct($name = 'Size', $mem = '512', $swap = '512', $cpu = '1', $disk = '10'){
        $this->mem = $mem;
        $this->swap = $swap;
        $this->cpu = $cpu;
        $this->disk = $disk;
        $this->name = $name;
    }

}

class Proxmox {

    protected $api_url;
    public $auth_data;

    public $driplets;

    public function __construct() {

        $this->api_url = $_ENV['proxmox_url'] . '/api2/json';

        $this->addDriplet(new Driplet('Smol', 512, 512, 1, 10));
        $this->addDriplet(new Driplet('Med', 1024, 1024, 2, 20));
        $this->addDriplet(new Driplet('Lurge', 4096, 1024, 6, 50));
    }

    public function addDriplet(Driplet $driplet) {
        $lower_name = strtolower($driplet->name);
        $this->driplets[$lower_name] = $driplet;
    }

    public function setAuthData() {

        if(!empty($_SESSION['auth_data'])) {
            $this->auth_data = $_SESSION['auth_data'];
        }
        return $this->auth_data;
    }

    public function authenticate($username, $password) {
        $this->auth_data = [];
        $r = new Request($this->api_url . '/access/ticket');
        $data = $r->post([
            'username' => $username . '@pam',
            'password' => $password
        ]);

        $this->auth_data = $data['data'];
        $_SESSION['auth_data'] = $this->auth_data;
        $_SESSION['auth_timeout'] = time();

        return $this->auth_data;
    }

    public function getContainers() {
        $r = new Request($this->api_url . '/cluster/resources');
        $r->setHeader('CSRFPreventionToken', $this->auth_data['CSRFPreventionToken']);
        $r->setHeader('Cookie', 'PVEAuthCookie='.$this->auth_data['ticket']);
        $resources = $r->get();

        return $resources['data'];
    }

	public static function formatBytes($bytes, $precision = 2) { 
	    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

	    $bytes = max($bytes, 0); 
	    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
	    $pow = min($pow, count($units) - 1); 

	    // Uncomment one of the following alternatives
	    //$bytes /= pow(1024, $pow);
	    $bytes /= (1 << (10 * $pow)); 

	    return round($bytes, $precision) . ' ' . $units[$pow]; 
	} 

    public function createContainer($data) {

        $r = new Request($this->api_url . '/nodes/s129183/lxc');
        $r->setHeader('CSRFPreventionToken', $this->auth_data['CSRFPreventionToken']);
        $r->setHeader('Cookie', 'PVEAuthCookie='.$this->auth_data['ticket']);

        $current_resources = $this->getContainers();
        $current_ids = [];
        foreach($current_resources as $resource) {
            if($resource['type'] == 'lxc') {
                $current_ids[] = explode('/', $resource['id'])[1];
            }
        }

        $vmid = max($current_ids) + 1;

        $ip = '10.0.0.' . (substr($vmid, 1) + 10) . '/8';

        $password = bin2hex(random_bytes(14));

        $payload = [
            'ostemplate' => 'local:vztmpl/ubuntu-16.04-x86_64.tar.gz',
            'vmid' => $vmid,
            'rootfs' => 'HDD:' . $data['disk'],
            'hostname' => $data['hostname'],
            'memory' => $data['mem'],
            'swap' => $data['swap'],
            'cores' => $data['cpu'],
            'net0' => 'name=eth0,bridge=vmbr0,gw=10.0.0.1,ip=' . $ip,
            'password' => $password
        ];

        $ret = $r->post($payload);
        $ret['password'] = $password;

        return $ret;
    }

}

class Controller {

    public $client;

    public function __construct() {
        $this->client = new Proxmox();
    }

    public function handle($get) {
        $ret = [];
        if(!empty($get['function'])) {
            header('Content-Type: application/json');

            switch($get['function']) {
                case 'create':
                    $this->client->setAuthData();
                    $container_type = strtolower($_POST['name']);
                    $container_data = json_decode(json_encode($this->client->driplets[$container_type]), true);
                    $container_data['hostname'] = $_POST['hostname'];

                    $ret = $this->client->createContainer($container_data);
                break;

                case 'authenticate':
                    $ret = $this->client->authenticate($_POST['username'], $_POST['password']);
                break;

                case 'logout':
                    unset($_SESSION['auth_data']);
                    $ret = ['yay' => 'didthething'];
                break;
            }

            echo json_encode($ret);
            exit;

        }
    }
}


    $app = new Controller();
    $app->handle($_GET);

    $containers = [];

    if(!empty($_SESSION['auth_timeout']) && time() - $_SESSION['auth_timeout'] > (60*60*24)) {
        unset($_SESSION['auth_data']);
    }
    if($app->client->setAuthData()){
        $containers = $app->client->getContainers();
    }

	usort($containers, function($a, $b){
		return $a['id'] <=> $b['id'];
	});

?>

<!DOCTYPE html>
<html>
<head>

    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">

    <style>

        h1 {
            color: #0069ff;
            font-family: 'Ubuntu', sans-serif;

        }

        .item {
            font-family: 'Ubuntu', sans-serif;
            width: 285px;
            height: 295px;
            background: #0069ff;
            text-align: center;
            padding: 5px;
            margin: 5px;
            float: left;
            color: #FFF;
        }

        .item h2 {
            color: #FFF;
            padding: 5px;
            text-align: center;
        }

        input {
            border: 1px solid #dfdfdf;
            font-size: 1rem;
            padding: 1rem;
            color: #444;
            border-radius: 3px;
            transition: all .2s ease-in-out;
            outline: 0;
            display: inline-block;
            margin-bottom: 1rem;
            text-align: left;
        }

        /*! CSS Used from: https://cloud-cdn-digitalocean-com.global.ssl.fastly.net/aurora/assets/aurora-6ace3c66c13d7f32db6fb21c361fe24d.css */
        .Button{-moz-transition:all .2s ease-in-out;-ms-transition:all .2s ease-in-out;}
        .Button{-webkit-touch-callout:none;-khtml-user-select:none;}
        button{font-family:inherit;font-size:100%;margin:0;}
        button{line-height:normal;}
        button{-webkit-appearance:button;cursor:pointer;}
        .Button{-webkit-appearance:none;}
        button::-moz-focus-inner{border:0;padding:0;}
        *,:after,:before{box-sizing:border-box;}
        .Button{cursor:pointer;}
        .Button{text-align:center;}
        .Button{display:inline-block;}
        .Button{font-weight:600;}
        .Button{border-radius:3px;}
        ::-moz-selection{background:#0069ff;color:#fff;}
        ::selection{background:#0069ff;color:#fff;}
        ::-moz-selection{background:#0069ff;color:#fff;}
        .Button{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;transition:all .2s ease-in-out;background:#ececec;color:#676767;border:1px solid #ececec;padding:0 1.6rem;font-size:.9rem;height:3rem;line-height:3rem;}
        .Button:not(.Button--noHover):hover{background:#dfdfdf;border:1px solid transparent;}
        .Button:focus{outline:0;box-shadow:0 0 0 1px #fff,0 0 2px 2px #0069ff;}
        .Button--fullWidth{width:100%;padding:0;}
        .Button--green{color:#fff;background:#15CD72;border:1px solid #15CD72;}
        .Button--green:not(.Button--noHover):hover{background:#0CB863;border:1px solid #0CB863;}
        ::-webkit-input-placeholder{color:#676767;}
        ::-moz-placeholder{color:#676767;}
        :-ms-input-placeholder{color:#676767;}
        ::placeholder{color:#676767;}
        :-moz-placeholder{color:#676767;}
        ::-moz-placeholder{color:#676767;}
        :-ms-input-placeholder{color:#676767;}

	.pure-table, table {
  border-collapse: collapse;
  -webkit-border-horizontal-spacing: 0px;
  -webkit-border-vertical-spacing: 0px;
}
.pure-table {
  empty-cells: show;
  border-top-width: 1px;
  border-right-width: 1px;
  border-bottom-width: 1px;
  border-left-width: 1px;
  border-top-style: solid;
  border-right-style: solid;
  border-bottom-style: solid;
  border-left-style: solid;
  border-top-color: rgb(203, 203, 203);
  border-right-color: rgb(203, 203, 203);
  border-bottom-color: rgb(203, 203, 203);
  border-left-color: rgb(203, 203, 203);
  border-image-source: initial;
  border-image-slice: initial;
  border-image-width: initial;
  border-image-outset: initial;
  border-image-repeat: initial;
}
* {
  box-sizing: border-box;
}
.pure-table thead {
  background-color: rgb(224, 224, 224);
  color: rgb(0, 0, 0);
  text-align: left;
  vertical-align: bottom;
}
legend, td, th {
  padding-top: 0px;
  padding-right: 0px;
  padding-bottom: 0px;
  padding-left: 0px;
}
.pure-table td, .pure-table th {
  border-left-style: solid;
  border-left-color: rgb(203, 203, 203);
  border-top-width: 0px;
  border-right-width: 0px;
  border-bottom-width: 0px;
  border-left-width: 1px;
  font-size: inherit;
  margin-top: 0px;
  margin-right: 0px;
  margin-bottom: 0px;
  margin-left: 0px;
  overflow-x: visible;
  overflow-y: visible;
  padding-top: 0.5em;
  padding-right: 1em;
  padding-bottom: 0.5em;
  padding-left: 1em;
}
.pure-table th, .pure-table td {
  padding-top: 0.5em;
  padding-right: 1em;
  padding-bottom: 0.5em;
  padding-left: 1em;
}
.pure-table td {
  background-color: transparent;
}
.pure-table-odd td, .pure-table-striped tr:nth-child(2n-1) td {
  background-color: rgb(242, 242, 242);
}

    </style>
</head>

<body>

<h1>Digital Sea</h1>

<?php if(empty($_SESSION['auth_data'])): ?>
<div>
    <label for="username">
        Username:
        <input id="username" type="text" placeholder="Username">
    </label>
</div>

<div>
    <label for="password">
        Password:
        <input id="password" type="password">
    </label>
</div>

<div>
    <button class="Button login">Login</button>
</div>

<?php else: ?>

    <div>
        <button class="Button logout">Logout</button>
    </div>

<?php endif; ?>

<table class="pure-table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Status</th>
			<th>RAM</th>
			<th>HDD</th>
		</tr>
	</thead>
<?php foreach($containers as $k => $container): ?>
    <?php if($container['type'] == 'lxc'): ?>
        <tr <?php if($k % 2 != 0): ?> class="pure-table-odd" <?php endif; ?>>
            <td><?=$container['name'];?></td>
            <td><?=$container['status'];?></td>
            <td><?=Proxmox::formatBytes($container['maxmem']);?></td>
            <td><?=Proxmox::formatBytes($container['maxdisk']);?></td>
            <?php //var_dump($container); ?>
   		</tr>
    <?php endif; ?>
<?php endforeach; ?>
</table>
<?php foreach($app->client->driplets as $driplet): ?>

    <div class="item">
        <h2><?php echo $driplet->name; ?> Driplet</h2>
        <div>
            <input type="text" id="hostname" placeholder="hostname">
        </div>

        <p>
            <?php echo $driplet->mem; ?> RAM / <?php echo $driplet->swap; ?>MB Swap
            <br />
            <?php echo $driplet->disk; ?>GB HDD
            <br />
            <?php echo $driplet->cpu; ?> CPU Core(s)
        </p>

        <button data-name="<?php echo $driplet->name; ?>" class="submit Button Button--green Button--large">Create</button>

    </div>

<?php endforeach; ?>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
    $('.submit').on('click', function() {
        $parent = $(this).closest('.item');
        var data = {
            name : $(this).data('name')
        };
        data['hostname'] = $parent.find('#hostname').val();

        if(data['hostname'] == '') {
            alert('Please enter a hostname!');
            return false;
        }

        $.ajax({
            url : '/?function=create',
            type : 'post',
            data : data,
            success : function(r) {
                console.log(r);
            }
        })
    });

    $('.login').on('click', function(){
        var data = {};
        data.username = $('#username').val();
        data.password = $('#password').val();

        $.ajax({
            url : '/?function=authenticate',
            type : 'post',
            data : data,
            success : function(r) {
                window.location.reload();
            }
        })
    });

    $('.logout').on('click', function(){

        $.ajax({
            url : '/?function=logout',
            success : function(r) {
                window.location.reload();
            }
        })
    });
</script>

</body>

</html>
